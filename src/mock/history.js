/**
 * Created by siller on 2017/8/10.
 */
let history = {
    "data": [{
        "_id": "2017-08-09",
        "fail": 28214970,
        "forbidden": 22556754,
        "get": 132430351,
        "success": 29286380,
        "total": 57371333
    }, {
        "_id": "2017-08-08",
        "forbidden": 46396261,
        "fail": 75243543,
        "success": 32647409,
        "total": 107932211,
        "get": 169828167
    }, {
        "_id": "2017-08-07",
        "fail": 48621137,
        "forbidden": 40173051,
        "total": 77115781,
        "get": 155040631,
        "success": 28800674
    }, {
        "_id": "2017-08-06",
        "total": 55444563,
        "get": 121952810,
        "fail": 32997470,
        "forbidden": 25698836,
        "success": 22786032
    }, {
        "_id": "2017-08-05",
        "forbidden": 35163947,
        "success": 24646152,
        "total": 64093472,
        "fail": 39776460,
        "get": 130896677
    }, {
        "_id": "2017-08-04",
        "fail": 41918047,
        "get": 143290487,
        "success": 27214068,
        "forbidden": 36879843,
        "total": 68782915
    }, {
        "_id": "2017-08-03",
        "fail": 50054806,
        "success": 27540053,
        "get": 164673831,
        "total": 77116112,
        "forbidden": 45753974
    }], "status": 1, "error_msg": "", "ex": 0
}

export default history;