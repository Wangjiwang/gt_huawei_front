
export default  function (data,hot) {
    let dataHot = {
        tooltip: {
            position: 'top',
            trigger: 'axis',
            backgroundColor: 'rgba(255,255,255,.9)',
            borderColor: '#A7A7A7',
            enterable: true,
            padding: 17,
            textStyle: {
                color: '#68728C'
            },
            extraCssText: 'box-shadow: 0 1px 3px 0 rgba(0,0,0,0.08);border-radius: 3px;font-size:12px;z-index:66',
            axisPointer: {
                lineStyle: {
                    color: '#A7a7a7',
                    type: "dashed"
                }
            },
            formatter: function (params) {
                let s = '';
                let tpl = 0;
                params.forEach(function (value) {
                    if (value.data[2] != '--') {
                        tpl += value.data[2]
                    }
                    return tpl
                });
                for (let i = 0; i < params.length; i++) {
                    let t, total;
                    if (isNaN((params[i].data[2]))) {
                        t = 0;
                        total = 0
                    } else {
                        t = params[i].data[2];
                        total = t/tpl
                    }
                    s += ['直接通过验证量', '滑动行为验证量', '图文行为验证量'][i] + ': ' + '<span style="margin-right: 5px ; min-width: 40px ;display: inline-block">' + t + '次</span>' + (total * 100).toFixed(2) + '%' + '<br/>'
                }
                if (params[0].axisValue) {
                    return '时间：' + params[0].name + '<br/>' + s
                }
                else {
                    return s
                }

            }
        },
        animation: false,
        grid: {
            y: '10%',
            x:'0%',
            top:'0px',
            right: '35px',
            left:'50px',
            containLabel:true
        },
        xAxis: {
            type: 'category',
            splitArea: {
                show: true
            },
            axisLine: {
                lineStyle: {
                    color: "#68728c"
                }
            },
            data:hot.date
        },
        yAxis: {
            type: 'category',
            data: ['直接通过', '滑动行为验证', '图文行为验证'],
            splitArea: {
                show: true
            },
            axisLine: {
                lineStyle: {
                    color: "#68728c"
                }
            },

        },
        visualMap: {
            min: 0,
            max:100,
            calculable: true,
            orient: 'vertical',
            left: '0',
            top: '15%',
            inRange:{
                color:["#f2f5ff","#a0c2f9","#71a3f7","#477ff7"]
            }
        },
        series: [{
            name: '成功量分布',
            type: 'heatmap',
            label: {
                normal: {
                    show: false
                }
            },
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            data:data.heat

        }]
    }
    return dataHot
}
