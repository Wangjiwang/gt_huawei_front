/**
 * Created by daishuang on 2018/4/24.
 */
import LineOption from "./LineOption"
import HeatOption from "./hotChart"
import BarOption from "./barChart"
import ForbiddenOption from "./TrangleChart"

let ChartOption = {
    LineOption,
    HeatOption,
    BarOption,
    ForbiddenOption
};

export default ChartOption