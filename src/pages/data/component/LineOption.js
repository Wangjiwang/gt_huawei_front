
export default function (baseData) {
    let option = {
        tooltip: {
            trigger: 'axis',
            backgroundColor: 'rgba(255,255,255,.9)',
            borderColor: '#A7A7A7',
            enterable: true,
            padding: 17,
            textStyle: {
                color: '#68728C'
            },
            extraCssText: 'box-shadow: 0 1px 3px 0 rgba(0,0,0,0.08);border-radius: 3px;font-size:12px;z-index:66',
            axisPointer: {
                lineStyle: {
                    color: '#A7a7a7',
                    type: "dashed"
                }
            },
        },
        legend: {
            bottom: '-5',
            left: '26',
            itemWidth: 14,
            itemHeight: 60,
            textStyle:{
                color:'#68728c'
            },
            data: [
                {
                    name: "验证请求量",
                    icon: 'circle',
                },
                {
                    name: '验证交互量',
                    icon: 'circle'
                },
                {
                    name: '验证通过量',
                    icon: 'circle'
                },
                {
                    name: '验证防御量',
                    icon: 'circle'
                }
            ]
        },
        grid: {
            left:'80px',
            right:'45px'
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            axisLabel: {
                margin: '20'
            },
            axisLine: {
                lineStyle: {
                    color: "#68728c"
                }
            },
            axisTick: {
                lineStyle: {
                    color: "#68728c"
                }
            },
            data: baseData.date
        },
        yAxis: [
            {
                axisLine: {
                    show: false,
                    lineStyle: {
                        color: "#68728c"
                    }
                },
                axisTick: {
                    show: false,
                    lineStyle: {
                        color: "#68728c"
                    }
                },
                name: '次数（次）',
                nameTextStyle:{
                    color:'rgba(104,114,140,.5)'
                },
                type: 'value',
                nameGap: '25',
                nameLocation: 'end',
                axisLabel: {
                    formatter: '{value}',
                    margin: '10',
                }
            }
        ],
        series: [
            {
                name: '验证请求量',
                type: 'line',
                smooth: true,
                data: baseData.register,
                itemStyle: {
                    normal: {
                        color: '#3E9CFC',
                        width: '4'
                    }
                }
            },
            {
                name: '验证交互量',
                type: 'line',
                smooth: true,
                data: baseData.total,
                itemStyle: {
                    normal: {
                        color: '#21B6CA',
                        width: '4'
                    }
                }
            },
            {
                name: '验证通过量',
                type: 'line',
                smooth: true,
                data: baseData.success,
                itemStyle: {
                    normal: {
                        color: '#40CE9F',
                        width: '4'
                    }
                }
            },
            {
                name: '验证防御量',
                type: 'line',
                smooth: true,
                data: baseData.fail,
                itemStyle: {
                    normal: {
                        color: '#F79647',
                        width: '4'
                    }
                }
            }
        ]
    };

    return option;
}