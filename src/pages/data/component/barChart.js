
export default function (data) {
    let  dataRisk = {
        tooltip: {
            trigger: 'axis',
            backgroundColor: 'rgba(255,255,255,.9)',
            borderColor: '#A7A7A7',
            enterable: true,
            padding: 17,
            textStyle: {
                color: '#68728C'
            },
            extraCssText: 'box-shadow: 0 1px 3px 0 rgba(0,0,0,0.08);border-radius: 3px;font-size:12px;z-index:66',
            axisPointer: {
                lineStyle: {
                    color: '#A7a7a7',
                    type: "dashed"
                }
            },
            formatter: function (params) {
                let s;
                s = params[0].seriesName + ': ' + '<span style="margin-right: 5px ; min-width: 40px ;display: inline-block">' + ['低','中','高'][params[0].data] + '</span>'
                return  '时间：' + params[0].name + '<br/>' + s
            }
        },
        calculable: true,
        grid: {
            y: '10%',
            x: '0%',
            top: '0px',
            right: '10px',
            left: '0px',
            bottom:'24px',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                axisLine: {
                    lineStyle: {
                        color: "#68728c"
                    }
                },
                data:data.time
            },

        ],
        yAxis: [
            {
                type: 'category',
                data: [ '低','中', '高'],
                axisLine: {
                    lineStyle: {
                        color: "#68728c"
                    }
                },
                axisTick: {
                    lineStyle: {
                        color: "#68728c"
                    }
                },
            }
        ],
        series: [
            {
                name: '验证异常等级趋势',
                type: 'bar',
                barMaxWidth:20,
                data: data.value,
                itemStyle: {
                    normal: {
                        color: '#3E9CFC',
                        width: '4'
                    }
                }
            },
        ]
    }
    return dataRisk
}
