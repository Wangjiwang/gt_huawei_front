export default function (data) {
    let dataForbidden = {
        title: {},
        tooltip: {
            backgroundColor: 'rgba(255,255,255,.9)',
            borderColor: '#A7A7A7',
            enterable: true,
            padding: 17,
            textStyle: {
                color: '#68728C'
            },
            extraCssText: 'box-shadow: 0 1px 3px 0 rgba(0,0,0,0.08);border-radius: 3px;font-size:12px;z-index:66',
            axisPointer: {
                lineStyle: {
                    color: '#A7a7a7',
                    type: "dashed"
                }
            },
            formatter: function (params) {
                let s = '';
                let len = params.data.value ;
                for (let i = 0; i < len.length; i++) {
                    if (len[i] != undefined) {
                        s += ['防模拟','防伪造','防暴力'][i] + ':' + len[i] + '%<br/>'
                    }
                    else {
                        s += ['防模拟','防伪造','防暴力'][i] + ':' + '暂无数据' + '<br/>'
                    }
                }
                return params.seriesName + '<br/>' + s
            }
        },
        legend: {},
        grid: {
            x: '0%',
            top: '0',
            right: '0',
            left: '0',
            bottom:'24',
            containLabel: true
        },
        radar: {
            indicator: [
                {name: '防模拟',max:100},
                {name: '防伪造',max:100},
                {name: '防暴力',max:100},
            ],
            axisLine: {
                lineStyle: {
                    color: '#68728C'
                }
            }
        },
        series: [{
            name: '防御偏量',
            type: 'radar',
            itemStyle: {normal: {areaStyle: {type: 'default'}}},
            data: [
                {
                    value: data,
                    itemStyle: {
                        normal: {
                            color: '#3E9CFC'
                        }
                    },
                },
            ]
        }]
    }
    return dataForbidden
}
