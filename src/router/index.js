import Vue from 'vue'
import Router from 'vue-router';
Vue.use(Router);

const Base = resolve => require(['../pages/base/base.vue'], resolve);
const Login = resolve => require(['../pages/login/login.vue'], resolve);


const Overview = resolve => require(['../pages/overview/overview'], resolve);
const DataCenterGlob = resolve => require(['../pages/overview/component/glob.vue'], resolve);

const DataOverview = resolve => require(['../pages/data/data'], resolve);
const DataChart = resolve => require(['../pages/data/component/data-chart'], resolve);

const Setting = resolve => require(['../pages/setting/setting'], resolve);
const BaseSetting = resolve => require(['../pages/setting/component/base-setting'], resolve);
// const SecuritySetting = resolve => require(['../pages/setting/component/security-setting'], resolve);
const UserCustomPic = resolve =>require(['../pages/userPic/userPic'],resolve)
const UserInfo = resolve =>require(['../pages/userinfo/userinfo'],resolve)


// 超级管理员
const SuperUser = reslove => require(['../pages/user/user'], reslove);
const UserAdd = reslove => require(['../pages/user/component/add-user'], reslove);
const Client =  reslove => require(['../pages/client/client'], reslove);
const ClientAdd = reslove => require(['../pages/client/component/add-client'], reslove);
const SharePic = reslove => require(['../pages/sharePic/sharePic'], reslove);
const CustomPic =  reslove => require(['../pages/customPic/customPic'], reslove);
const SafeSet = reslove => require(['../pages/safeSet/safeSet'],reslove);
const AuthInfo = resolve =>require(['../pages/userinfo/authinfo'],resolve)


// 自定义图集
const WaitPic = reslove => require(['../pages/customPic/component/waitPic'], reslove);
const CuttedPic = reslove => require(['../pages/customPic/component/cuttedPic'], reslove);
const FailPic = reslove => require(['../pages/customPic/component/failPic'], reslove);
const RefusePic = reslove => require(['../pages/customPic/component/refusePic'], reslove);
const SearchPic =  reslove => require(['../pages/customPic/searchPic'], reslove);
const AllCheckPic =  reslove => require(['../pages/customPic/customeAllPic'], reslove);

// demo展示页面
const DemoCap = reslove => require(['../pages/capDemo/capDemo'], reslove);


// viewTwo viewThree viewFive groupTwo groupThree 定义的是chunk名

const routes = [
    {
        path: '/login',
        component: Login,
    },
    {
        path: '/demo',
        component :DemoCap,
    },
    {
        path: '/base',
        component: Base,
        children: [
            {
                path: 'overview',
                component: Overview,
                children: [
                    {
                        path: '',
                        component: DataCenterGlob,
                        name: 'glob'
                    },
                ]
            },
            {
                path: 'data',
                component: DataOverview,
                children: [
                    {
                        path: '',
                        component: DataChart,
                        name: 'data-chart'
                    },
                ]
            },
            {
                path: 'setting',
                component: Setting,
                children: [
                    {
                        path: '',
                        component: BaseSetting,
                        name: 'base-setting'
                    }
                ]
            },
            {
                path: 'user',
                component: SuperUser,
                children: [
                    {
                        path: '',
                        component: UserAdd,
                        name: 'user-add'
                    },
                ]
            },
            {
                path: 'client',
                component: Client,
                children: [
                    {
                        path: '',
                        component: ClientAdd,
                        name: 'client-add'
                    },
                ]
            },
        
            {
                path: 'userinfo',
                component: UserInfo,
                name:'userinfo'
            },
            {
                path:'authinfo',
                component: AuthInfo,
                name: 'authinfo'

            },
            {
                path:'sharePic',
                component:SharePic,
                name:'share-pic'
            },
            {
                path:'customPic',
                component:CustomPic,
                children:[
                    {
                        path:'wait',
                        component: WaitPic,
                        name:'wait'
                    },
                    {
                        path:'cutted',
                        component: CuttedPic,
                        name:'cutted'
                    },
                    {
                        path:'fail',
                        component:FailPic,
                        name:'fail',
                    },
                    {
                        path:'refuse',
                        component:RefusePic,
                        name:'refuse'
                    },
                    {
                        path:'',
                        component: WaitPic,
                    }
                ]

            },
            {
                path:'searchPic',
                component: SearchPic,
                name:'searchPic'
            },
            {
                path:'safeSet',
                component: SafeSet,
                name:'safeSet'
            },
            {
                path:'checkedPic',
                component: AllCheckPic,
                name: 'checkedPic'
            }, 
            {
                path:'picture',
                component:UserCustomPic,
                name:'picture'
            },
            
            {
                path: '',
                redirect: '/base/user'
            }
        ]
    },
    {
        path: '',
        redirect: '/base'
    },
    
];

export default new Router({
    mode: 'history',
    scrollBehavior: () => ({y: 0}),
    routes
})
