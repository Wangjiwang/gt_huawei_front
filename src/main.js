
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';
import Cookies from 'js-cookie';
import 'normalize.css/normalize.css';// normalize.css 样式格式化
import MuseUI from 'muse-ui'; //目前是全部加载插件，如果后面使用的少就改为一个个单独引入
import 'muse-ui/dist/muse-ui.css'
import 'styles/index.less'; // 全局自定义的主题样式
import 'assets/fonts/icon.css';
import * as filters from './filtres'; // 全局vue filter
import VueClipboard from 'vue-clipboard2'


Vue.use(MuseUI);
Vue.use(VueClipboard)
Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
});
router.beforeEach((to, from , next)=>{
    let isSuper = Cookies.get('is_superuser') || 'False'
    let blackList = ['user-add','client-add','share-pic','wait', 'cutted','fail', 'refuse','searchPic','checkedPic'];
    let whiteList = ['glob','data-chart','base-setting', 'userinfo','picture']
    let Islogin = Cookies.get('session_id');
    // let isSuper = 'False'
    // 黑名单只允许超管进入页面；
    if(!Islogin && to.path !=='/login'&& to.path !=='/demo'){
        return next({path:'/login'})
    }else {
        if(isSuper == 'False'){
            // 不能进入黑名单页面
            if(blackList.indexOf(to.name)>=0){
                return next({path:'/base/overview'})
            }
        }
    }
    next()
})

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');






