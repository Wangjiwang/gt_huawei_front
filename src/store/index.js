/**
 * Created by daishuang on 2017/6/19.
 */
import Vue from 'vue';
import Vuex from 'vuex';
import Cookies from 'js-cookie'
import {merchantsApi, captchasApi, boardInfoApi} from 'api/board';
import {login, logOut} from 'api/login'
import {auth} from 'api/auth'

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {},
    state: {
        sideWidth: true,
        user_id: {},
        merchantCurrent: {},
        captachCurrent:{}

        
    },
    getters: {
        sideWidth: state => state.sideWidth,
        is_Super: state => state.is_Super,
        cid: state => {
            if(state.cid == 0){
                state.cid = 1;
            }
        }
    
    },
    mutations: {
        SET_SIDEWIDTH: (state) => {
            state.sideWidth = !state.sideWidth;
        },

        IS_SUPERUSER: (state) => {
            console.log(state);
        },

        updateMerchantCurrent(state, data) {
            state.merchantCurrent = data
        },
       
        updateCaptchaCurrent(state, data){
            state.captachCurrent = data
        }

       

    },

    actions: {
        Login({commit}, inputInfo) {
            return new Promise((resolve, reject) => {
                login(inputInfo).then(
                    res => {
                        if (res.data.status === 1) {
                            let res_data = res.data.data;
                            console.log(res_data);
                        }
                        resolve(res);
                    }
                )
            })
        },
        LoginOut({commit}){
            logOut().then(
                res => {
                    Cookies.remove('user_id');
                    Cookies.remove('is_superuser');
                }
            )
        },

    }
});

export default store