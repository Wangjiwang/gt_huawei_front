/**
 * Created by daishuang on 2017/7/4.
 */
export function formatData(data) {
    if(data){
        if(data.length>=9){
            return data.substr(0, 4) + "***" + data.slice(-4)
        }
        else {
            return data
        }
    }
    else {
        return
    }
}

export function formateDate(date) {
    var o = {
        "y+": this.getFullYear(),
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
    };
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(date)){
            if(k == "y+"){
                date = date.replace(RegExp.$1, ("" + o[k]).substr(4 - RegExp.$1.length));
            }
            else if(k=="S+"){
                var lens = RegExp.$1.length;
                lens = lens==1?3:lens;
                date = date.replace(RegExp.$1, ("00" + o[k]).substr(("" + o[k]).length - 1,lens));
            }
            else{
                date = date.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
    }
    return date;
}

export function formateSum(sum){
    if(sum) {
        sum = sum.toString();
        return sum.replace(/(?=(?!^)(?:\d{3})+(?:\.|$))(\d{3}(\.\d+$)?)/g,',$1');
    }else{
        return '0'
    }
}

export function formatSub(data) {
    if(data){
        return data.substring(0,10);
    }
    else {
        return
    }
}

export function changeTimeFormata(data) {
    if(data){
        data = data.toString()
        let index = data.split('T')
        return index[0]
    }else{
        return ''
    }
}

export function changeTimeFormataTime(data) {
    if(data){
        data = data.toString()
        let t = data.split('.');
        let index
        if(t[0]){
            index = t[0].split('T')
        }
        return index[0] + '  ' +  index[1]
    }else{
        return ''
    }
}