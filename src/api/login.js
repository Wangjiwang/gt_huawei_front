import share from 'api/share'
export function login(data) {
    // const data = {email:email};
    return share({
        url: '/api/login',
        method: 'post',
        data
    });
}
export function logOut() {
    return share({
        url: '/api/logout',
        method: 'get',
    });
}