import share from 'api/share';

export function imgsApi(data) {
  return share({
    url: '/api/pictures',
    method: 'post',
    data
  });
}

export function imgSelectApi(data) {
  return share({
    url: '/api/picture/select',
    method: 'post',
    data
  });
}

export function imgCancelApi(data) {
  return share({
    url: '/api/picture/cancel',
    method: 'post',
    data
  });
}

export function showAiApi(data) {
    return share({
        url: '/api/captcha/show_ai',
        method: 'post',
        data
    });
}

export function setAiApi(data) {
    return share({
        url: '/api/captcha/set_ai',
        method: 'post',
        data
    });
}


