import https from 'api/share'

export function merchantsApi(data) {
    return https ({
        url: '/api/merchants',
        method: 'post',
        data
    })
}
export function captchasApi(data) {
    return https({
        url: '/api/captchas',
        method: 'post',
        data
    })
}
export function boardInfoApi(data) {
    return https({
        url: '/api/captcha',
        method: 'post',
        data
    })
}

//获取用户关联的已启用的商户信息
export function merchantsChecked(data) {
    return https ({
        url: '/api/admin/user/merchant/list',
        method: 'post',
        data
    })
}