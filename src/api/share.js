import axios from 'axios';
import store from '../store';
//0.119
// 创建axios实例
const service = axios.create({
    baseURL: process.env.BASE_API, // api的base_url
    // baseURL: "http://192.168.0.119:8000", // api的base_url
    // baseURL: "http://192.168.0.48:8000", // api的base_url
    //timeout: 5000                  // 请求超时时间
});
axios.defaults.headers.post['Content-Type']="application/x-www-from-urlencoded";
axios.defaults.withCredentials=true;
// request拦截器
// service.interceptors.request.use(config => {
//     // config.xhr = { withCredentials: true };
//     // config.options.request.withCredentials =true
//     // config.setHeader("Access-Control-Allow-Credentials", "true");
//     return config;
//
// }, error => {
//     // Do something with request error
//     console.log(error); // for debug
//     Promise.reject(error);
// })
//
// respone拦截器
// service.interceptors.response.use(
//     response => response,
//     error => {
//         console.log('err' + error);// for debug
//         return Promise.reject(error);
//     }
// )

export default service;
