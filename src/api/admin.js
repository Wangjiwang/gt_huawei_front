/**
 * Created by daishuang on 2017/7/6.
 */
import share from 'api/share'
// 添加用户
export function AddUser(data) {
    return share({
        url: '/api/admin/user/create',
        method: 'post',
        data
    });
}

// 添加商户 
export function AddMerchant(data) {
    return share({
        url: '/api/admin/merchant/create',
        method: 'post',
        data
    });
}
// 获取所有用户信息
export function getAllUser(data) {
    return share({
        url: '/api/admin/user/all',
        method: 'post',
        data
    });
}
// 获取所有商户信息
export function getAllMerchant() {
    return share({
        url: '/api/admin/merchant/all',
        method: 'post',
        data:''
    });
}
// 获取用户下的所有商户
export function getAllUserMerchant(data) {
    return share({
        url: '/api/admin/user/merchant/list',
        method: 'post',
        data
    });
}
// 权限配备
export function UserBindMerchant(data) {
    return share({
        url: '/api/admin/user/configure',
        method: 'post',
        data
    });
}
// 解绑商户
export function unlockMerchantUser(data) {
    return share({
        url: '/api/admin/user/merchant/unbind',
        method: 'post',
        data
    });
}


// 二期
// 搜索用户

export function UserSearch(data) {
    return share({
        url: '/api/admin/user/search',
        method: 'post',
        data
    });
}

// 编辑用户信息

export function UsereditInfo(data) {
    return share({
        url: '/api/admin/user/edit',
        method: 'post',
        data
    });
}
// 删除用户

export function deleteUsers(data) {
    return share({
        url: '/api/admin/user/delete',
        method: 'post',
        data
    });
}

// 用户启用/禁用
export function UserContral(data) {
    return share({
        url: '/api/admin/user/activate',
        method: 'post',
        data
    });
}

// 查找商户

export function MerchantSearch(data) {
    return share({
        url: '/api/admin/merchant/search',
        method: 'post',
        data
    });
}

// 编辑商户信息 

export function MerchantEdit(data) {
    return share({
        url: '/api/admin/merchant/edit',
        method: 'post',
        data
    });
}

// 删除商户

export function MerchantDelete(data) {
    return share({
        url: '/api/admin/merchant/delete',
        method: 'post',
        data
    });
}

// 商户禁用/启用

export function MerchantContral(data) {
    return share({
        url: '/api/admin/merchant/activate',
        method: 'post',
        data
    });
}

// 商户禁用规则

export function MerchantforbiddenRule(data) {
    return share({
        url: '/api/admin/merchant/captcha/statistics',
        method: 'post',
        data
    });
}

// 获取已启用的商户列表
export function merchantsActiveData(data) {
    return share({
        url: '/api/admin/merchant/all/active',
        method: 'post',
        data
    })
}
// 获取公司列表 
export function  getallCompanyName() {
    return share({
        url: '/api/admin/company/list',
        method: 'get',
    })
}