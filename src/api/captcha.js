import https from 'api/share'

export function captchaApiSlide() {
    return https ({
        url: '/api/demo/slide',
        method: 'get',
    })
}
export function captchaApiClick() {
    return https ({
        url: '/api/demo/click',
        method: 'get',
    })
}
// 二次验证 slide

export function captchaSecondValidate(data) {
    return https ({
        url: '/api/demo/validate',
        method: 'post',
        data
    })
}
// 二次验证 click
export function captchaSecondValidateClick(data) {
    return https ({
        url: '/api/demo/validate',
        method: 'post',
        data
    })
}