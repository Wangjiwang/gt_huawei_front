import share from 'api/share'
// 获取用户信息
export function userInfo(data) {
    return share({
        url: '/api/user',
        method: 'post',
        data
    });
}
// 获取用户下所有商户
export function userMerchants(data) {
    return share({
        url: '/api/merchants',
        method: 'post',
        data
    });
}

// 获取商户下所有captcha信息
export function merchantsCap(data) {
    return share({
        url: '/api/captchas',
        method: 'post',
        data
    })
}

// 获取商户下所有captcha 验证信息
export function merchantsCapData(data) {
    return share({
        url: '/api/captcha/merchant_report',
        method: 'post',
        data
    })
}

// 删除商户下所有captcha
export function merchantsDeleteCap(data) {
    return share({
        url: '/api/captcha/delete',
        method: 'post',
        data
    })
}

// 新增商户下所有captcha
export function merchantsAddCap(data) {
    return share({
        url: '/api/captcha/add',
        method: 'post',
        data
    })
}

// 获取用户下所有ID的昨日验证量
export function merchantsAllCapData(data) {
    return share({
        url: '/api/captcha/userallvalidate',
        method: 'post',
        data
    })
}

// 获取用户下的预警信息
export function merchantsAlertData(data) {
    return share({
        url: '/api/captcha/userforbiddencount',
        method: 'post',
        data
    })
}

