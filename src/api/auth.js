/**
 * Created by daishuang on 2017/6/21.
 */
import share from 'api/share'
export function authApi() {
    return share({
        url: '/api/auth',
        method: 'get',
    });
}

export function updataValidateApi() {
    return share({
       url: '/api/validcode',
        method: 'get'
    })
}

export function isLogined() {
    return share({
       url: '/api/islogined',
        method: 'get'
    })
}

// 判断是否是生产环境
export function isPordEvn() {
    return share({
        url: 'api/env',
        method: 'get',
    });
}

// 管理员获取默认验证类型

export function getDefaultTpye(data) {
    return share({
        url: '/api/admin/verify/current',
        method: 'post',
        data
    });
}

// 管理员配置默认验证类型

export function setDefaultTpye(data) {
    return share({
        url: '/api/admin/verify/config',
        method: 'post',
        data
    });
}

// 模型版本
export function setDefaultModel(data) {
    return share({
        url: '/api/admin/cnn/config',
        method: 'post',
        data
    });
}
// 获取当前模型版本
export function getDefaultModel(data) {
    return share({
        url: '/api/admin/cnn/current',
        method: 'post',
        data
    });
}
// 获取忘记密码地址

export function getDefaultUrl() {
    return share({
        url: '/api/link',
        method: 'get',
    });
}