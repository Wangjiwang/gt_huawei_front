/**
 * Created by daishuang on 2017/7/18.
 */
import share from 'api/share'
export function Search(data) {
    return share({
        url: '/api/search',
        method: 'post',
        data
    });
}