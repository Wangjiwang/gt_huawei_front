/**
 * Created by daishuang on 2018/7/6.
 */
import share from 'api/share'
export function commonUpload(data) {
    return share({
        url: '/api/admin/image/common/upload',
        method: 'post',
        data
    });
}
// 获取公有图集列表
export function getCommonPicList(data) {
    return share({
        url: '/api/admin/image/common/all',
        method: 'post',
        data
    });
}
// 公有图集的启用和禁用 
export function commonPicContral(data) {
    return share({
        url: '/api/admin/image/common/activate',
        method: 'post',
        data
    });
}
// 公有图集切图 
export function commonPicCut(data) {
    return share({
        url: '/api/admin/image/common/cut',
        method: 'post',
        data
    });
}


// 自定义图集待审核列表
export function customPicWait(data) {
    return share({
        url: '/api/admin/image/verify/wait',
        method: 'post',
        data
    });
}
// 自定义图集已审核列表

export function customPicPass(data) {
    return share({
        url: '/api/admin/image/verify/pass',
        method: 'post',
        data
    });
}
// 自定义图集已拒绝
export function customPicRefuse(data) {
    return share({
        url: '/api/admin/image/verify/refuse',
        method: 'post',
        data
    });
}
// 自定义图集失败
export function customPicFail(data) {
    return share({
        url: '/api/admin/image/verify/fail',
        method: 'post',
        data
    });
}

// 用户自定义图集上传图集
export function customSelfUpload(data) {
    return share({
        url: '/api/admin/image/custom/upload',
        method: 'post',
        data
    });
}

// 获取用户商户下自定义列表 

export function customMerchantPicList(data) {
    return share({
        url: '/api/admin/image/custom/all',
        method: 'post',
        data
    });
}
// 获取商户下已启用的图集 /api/admin/image/custom/all/active
export function customMerchantActivePicList(data) {
    return share({
        url: '/api/admin/image/custom/all/active',
        method: 'post',
        data
    });
}
// 自定义图集详情 

export function customSelfPicDetail(data) {
    return share({
        url: '/api/admin/image/custom/detail',
        method: 'post',
        data
    });
}
// 管理员自定义图集审核图集

export function adminSelfPicCut(data) {
    return share({
        url: '/api/admin/image/custom/cut',
        method: 'post',
        data
    });
}
// 管理员自定义图集拒绝 

export function adminSelfPicRefuse(data) {
    return share({
        url: '/api/admin/image/custom/refuse',
        method: 'post',
        data
    });
}
//管理员查看已拒绝图集详情
export function adminSelfRefuseDeatil(data) {
    return share({
        url: '/api/admin/image/verify/detail',
        method: 'post',
        data
    });
}

//管理员搜索自定义图集  

export function adminSelfSearch(data) {
    return share({
        url: '/api/admin/image/custom/search',
        method: 'post',
        data
    });
}

// 审核记录 
export function adminAllCheckedPic(data) {
    return share({
        url: '/api/admin/image/verify/record',
        method: 'post',
        data
    });
}

// 商户下已审核图集 
export function customerCheckedPic(data) {
    return share({
        url: '/api/admin/image/custom/captcha/all',
        method: 'post',
        data
    });
}

// 自定义图集的启用和禁用 
export function customerIsUsed(data) {
    return share({
        url: '/api/admin/image/custom/activate',
        method: 'post',
        data
    });
}
// 使用图集& 取消使用

export function customerChoosePic(data) {
    return share({
        url: '/api/admin/image/enforce',
        method: 'post',
        data
    });
}

export function customerCanclePic(data) {
    return share({
        url: '/api/admin/image/unenforce',
        method: 'post',
        data
    });
}


// 获取公有已审核图集列表
export function getCommonPicChecked(data) {
    return share({
        url: '/api/admin/image/common/all/active',
        method: 'post',
        data
    });
}

// 共有图集删除
export function CommonPicDel(data) {
    return share({
        url: '/api/admin/image/common/delete',
        method: 'post',
        data
    });
}
// 自定义图集删除

export function CustomPicDel(data) {
    return share({
        url: '/api/admin/image/custom/delete',
        method: 'post',
        data
    });
}
