import https from 'api/share'
// 数据统计 表格
export function linecharts(data) {
    return https ({
        url: '/api/captcha/report',
        method: 'post',
        data
    })
}

export function hotcharts(data) {
    return https ({
        url: '/api/captcha/success',
        method: 'post',
        data
    })
}



export function riskcharts(data) {
    return https ({
        url: '/api/captcha/risklevel',
        method: 'post',
        data
    })
}

export function forbiddencharts(data) {
    return https ({
        url: '/api/captcha/forbidden',
        method: 'post',
        data
    })
}
// 月验证时间

export function passtimecharts(data) {
    return https ({
        url: '/api/captcha/passtime',
        method: 'post',
        data
    })
}
